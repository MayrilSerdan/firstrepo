<?php
Route::get('/', function () {
    return view('welcome');
});
 
Auth::routes();
 
Route::get('/home', 'HomeController@index')->name('home');
 
Route::post('/post', 'PostController@post');
 
Route::get('/show', 'PostController@getPost');